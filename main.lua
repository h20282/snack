queue = {}
function queue.new()
	return {
		front_idx = 1,
		tail_idx = 1
	}
end

function queue.push(q, item)
	q[q.front_idx] = item
	q.front_idx = q.front_idx + 1
end

function queue.pop(q)
	item = q[q.tail_idx]
	q[q.tail_idx] = nil
	q.tail_idx = q.tail_idx + 1
	return item
end

function queue.front(q)
	return q[q.front_idx-1]
end

function queue.size(q)
	return q.front_idx-q.tail_idx
end


local snack_nodes = queue.new()
queue.push(snack_nodes,{0,0})

local msg_queue = queue.new()


local snack_positions = {{0,0}}
local snack_len = 1
local front_idx = 2
local tail_idx = 1

local game_size = 20
local cell_size = 30
local curr_dir = 1
local timer = 0

local food_x = 1
local food_y = 0

local _xy = {0,1,0,-1,0}
local key_to_dir = {
	["down"] = 1,
	["right"] = 2,
	["up"] = 3,
	["left"] = 4
}

function queue_push(pos)
	snack_positions[front_idx] = pos
	front_idx = front_idx + 1
end

function queue_pop()
	pos = snack_positions[tail_idx]
	snack_positions[tail_idx] = nil
	tail_idx = tail_idx+1
	return pos
end

function queue_front()
	return snack_positions[front_idx-1]
end

function love.load()
	love.window.setMode(game_size*cell_size, game_size*cell_size)
	love.graphics.setBackgroundColor(255,255,255,100)
end

function love.keypressed(key)
	for k,dir in pairs(key_to_dir) do
		if k==key then
			print('push', dir)
			queue.push(msg_queue, dir)
		end
		-- if k==key and curr_dir%2 ~= dir%2 then
		-- 	curr_dir = dir
		-- end
	end
end

function in_snake(fx, fy)
	-- local line = ''
	for idx, pos in pairs(snack_positions) do
		-- line = line .. string.format(' (%d,%d)', pos[1], pos[2])
		if pos[1]==fx and pos[2]==fy then return true end
	end
	-- print(line)
	return false
end

function game_over()
	flag_game_over = true
end

function update_snack()
	local pos = queue_front()
	while queue.size(msg_queue)>0 and queue.front(msg_queue) %2 == curr_dir%2 do
		queue.pop(msg_queue)
	end
	if queue.size(msg_queue) > 0 then 
		curr_dir = queue.front(msg_queue)
		queue.pop(msg_queue)
	end
	local _x = _xy[curr_dir]
	local _y = _xy[curr_dir+1]
	-- curr_dir = curr_dir+1; if curr_dir > 4 then curr_dir = 1 end
	local next_x, next_y = pos[1] + _x, pos[2] + _y
	next_x = (next_x+game_size) % game_size
	next_y = (next_y+game_size) % game_size
	if in_snake(next_x, next_y) then
		game_over()
	end
	queue_push({next_x, next_y})
	if next_x==food_x and next_y == food_y then -- eat food, rand a food
		snack_len = snack_len + 1
		repeat
			food_x = math.random(0, game_size-1)
			food_y = math.random(0, game_size-1)
		until not in_snake(food_x, food_y)
	else
		queue_pop()
	end
end

function love.update( dt )
	if flag_game_over then return end

	timer = timer+dt
	if timer > 0.3 - math.log10(snack_len)/10 and not flag_game_over then
		update_snack()
		timer = 0
	end
end

function love.draw()
	love.graphics.setColor(1, 153/255, 85/255, 1)
	for idx, pos in pairs(snack_positions) do
		x, y = pos[1], pos[2]
		-- print(x, y)
		love.graphics.rectangle("fill", x*cell_size, y*cell_size, cell_size, cell_size);
	end
	love.graphics.setColor(1, 0, 0, 1)
	love.graphics.rectangle("fill", food_x*cell_size, food_y*cell_size, cell_size, cell_size);
end
